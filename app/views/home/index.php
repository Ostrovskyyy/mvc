<?php

	session_start();

 ?>

<html>
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Bartosz Ostrowski" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
	
	<title>Wypożyczalnia samochodów</title>
	
</head>
<body>

		<!-- navigation bar -->
	
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Wypożyczalnia samochodów</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="lista.php">Lista aut</a></li>
        <li><a href="#">Kontakt</a></li>
        
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
<?php
	if (isset($_SESSION['logged']) && $_SESSION['logged']=true)
	{
		echo '<li><a href="logout.php">Wyloguj się</a></li>';
	}
	 else{
        echo '<li><a href="login.php">Zaloguj się</a></li>';
		echo '<li><a href="register.php">Zarejestruj się</a></li>';
	 }
	  
?>
	  </ul>
    </div>
  </div>
</nav>
 
		<!-- container -->

	<div class="jumbotron">
	<center>
	  <h1>
	  <?php 
	  
	  if(isset($_SESSION['user']))
	  {
			echo "Witaj w wypożyczalni, ".$_SESSION['user']."!";
	  }
	  else
	  {
			echo "<p>Witaj w wypożyczalni! Zaloguj się lub przejdź do listy samochodów</p>";
	  }
	  
	  ?>
	  </h1>
	  <p></p>
	  </center>
	</div>




</body>
</html>