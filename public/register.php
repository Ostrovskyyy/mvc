<?php 

	session_start(); 

	
	if (isset($_SESSION['logged']) && $_SESSION['logged']=true)
	{
		header('Location: index.php');
		exit();
	}

	if (isset($_POST['login']))
	{
		$validation = true;
		
	//password check
		$password = $_POST['password'];
		if ((strlen($password)<5) || (strlen($password)>20))
		{
			$validation = false;
			$_SESSION['e_password']='<span style="color:red">Hasło musi posiadać od 5 do 20 znaków</span>';
		}
		
	//login check
		$login = $_POST['login'];
		if ((strlen($login)<3) || (strlen($login)>15))
		{
			$validation = false;
			$_SESSION['e_login']='<span style="color:red">Login musi posiadać od 3 do 15 znaków</span>';
		}
			
	}

	require_once '../app/models/connector.php';
	
	$result = @$pdo->query("SELECT user_id FROM `users` WHERE login='$login'");
	
	if (!$result) throw new Exception($pdo->error);
	
	$no_logins = $result->rowCount();
	if($no_logins>0)
	{
		$validation=false;
		$_SESSION['e_login']='<span style="color:red">Podany login już istnieje</span>';
	}
	
	if (@$validation == true)
	{
		if ($pdo->query("INSERT INTO `users` VALUES (NULL, '$login', '$password', 'NIE')"))
		{
			$_SESSION['registerdone']=true;
			$_SESSION['registerdoneinfo']='<span style="color:green">Rejestracja przebiegła pomyślnie</span>';
			
			$result=null;
		}
		else
		{
			echo 'Błąd krytyczny xD';
		}
	}
	
	
?>

<html>
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Bartosz Ostrowski" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
	
	<title>Wypożyczalnia samochodów</title>
	
</head>
<body>

		<!-- navigation bar -->
	
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Wypożyczalnia samochodów</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="lista.php">Lista aut</a></li>
        <li><a href="#">Kontakt</a></li>
        
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
<?php
	if (isset($_SESSION['logged']) && $_SESSION['logged']=true)
	{
		echo '<li><a href="logout.php">Wyloguj się</a></li>';
	}
	 else{
        echo '<li><a href="login.php">Zaloguj się</a></li>';
		echo '<li><a href="register.php">Zarejestruj się</a></li>';
	 }
	  
?>
	  </ul>
    </div>
  </div>
</nav>
 
		<!-- container -->

	<div class="jumbotron">
	<center>
	  <h1>
	  Rejestracja <br/><br/>
	  </h1>
	  <p>
	  
		<form method="post">
		
				Login: <br/> <input type="text" name="login" /> <br/><br/>
				Hasło: <br/> <input type="password" name="password" /> <br/><br/>
		
		
				<input type="submit" value="Zarejestruj" />
		
		</form>
	  
		<?php
			//error pass
				if(isset($_SESSION['e_password']))
				{
				echo $_SESSION['e_password'].'<br/>';
				}
			//error login
				if(isset($_SESSION['e_login']))
				{
				echo $_SESSION['e_login'];
				}
			//register pass
				if(isset($_SESSION['registerdone']))
				{
					echo $_SESSION['registerdoneinfo'];
				}
			?>
	  
	  </p>
	  </center>
	</div>



</body>
</html>